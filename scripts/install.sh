#!/bin/bash

cd gateway && yarn && cd - || exit
cd microservices/tariff-svc && yarn && cd - || exit
cd microservices/valorization-svc && yarn && cd - || exit
