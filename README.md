# API - Trebol Demo

## Instalación

Puedes realizar la instalación de todas las dependencias en todos los proyectos que componen la `API` por medio del script `install.sh` que se encuentra en la carpeta `./scripst/install.sh`

Una vez instaladas todas la dependencias puedes entrar a cada una de las carpetas de los proyectos y ejecuar el comando `yarn start:dev ó npm run start:dev` para poner en ejecución la `API`.

